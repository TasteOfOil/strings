import java.util.Scanner;
import java.util.HashMap;
import java.util.Vector;




public class Main {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        OOP(scan);
        MinWord(scan);
        findLatWord(scan);
        findPalindrom(scan);


    }

    public static void OOP(Scanner scan){
        String str = scan.nextLine();

        StringBuilder str_b = new StringBuilder(str);

        String str_search = "object-oriented programming";
        String str_replace = "OOP";
        int count = 0;
        for(int i = 0;i<str_b.length();i++){
            if(str_b.charAt(i) == 'o' ||
                    str_b.charAt(i) == 'O'){
                if(str_search.regionMatches(true,0,str,i,str_search.length())==true){
                    if(count==1){
                        str_b = str_b.replace(i, i+str_search.length(), str_replace);
                        str = String.valueOf(str_b);
                        count = 0;
                        i = i+3;
                    }
                    else{
                        count++;
                    }
                }
            }
        }
        System.out.println(str_b);
    }

    public static void MinWord(Scanner scan){
        String str = scan.nextLine();
        HashMap<Integer, Integer> map = new HashMap<>();
        Vector word = new Vector();
        int cnt;
        int j;
        for(int i = 0;i<str.length();i++){
            if(i==0||str.charAt(i)==' '){
                if(str.charAt(i)==' '){
                    i++;
                }
                for( j = i;j<str.length();j++){
                    if(str.charAt(j)==' '){
                        break;
                    }
                    if(word.indexOf(String.valueOf(str.charAt(j)))==-1){
                        word.addElement(String.valueOf(str.charAt(j)));
                    }

                }
                cnt = word.size();
                map.put(i, cnt);
                word.clear();
                i=j-1;
            }

        }

        Integer min = 0, temp, min_i = -1;
        for(HashMap.Entry entry: map.entrySet()){
            if(min_i==-1){
                min = (Integer)(entry.getValue());
                min_i=(Integer)(entry.getKey());
            }
            else{
                temp = (Integer)(entry.getValue());
                if(temp<min){
                    min = temp;
                    min_i=(Integer)(entry.getKey());
                }
            }
        }
        for(int i = min_i;i<str.length();i++){
            if(str.charAt(i)!=' '){
                System.out.printf(String.valueOf(str.charAt(i)));
            }
            else{
                break;
            }
        }
    }

    public static void findLatWord(Scanner scan){
        String str = scan.nextLine();
        int cnt = 0;
        int j =0;
        for(int i = 0;i<str.length();i++){
            if(i==0 || str.charAt(i)==' '){
                if(str.charAt(i)==' '){
                    i++;
                }
                cnt++;
                for(j = i;j<str.length();j++){
                    if(str.charAt(j)==' '){
                        break;
                    }
                    if((str.charAt(j)<'A'||str.charAt(j)>'Z')&&(str.charAt(j)<'a'||str.charAt(j)>'z')){
                        cnt--;
                        break;
                    }
                }
                i = j-1;
            }

        }
        System.out.println("The number of words containing only letters of the Latin alphabet is "+cnt);
    }

    public static void findPalindrom(Scanner scan){
        String str = scan.nextLine();
        String[] words = str.split(" ");
        for (String word : words) {
            System.out.println(word);
        }
        boolean flag = true;
        System.out.println("Palindrome words: ");
        for(int i = 0;i<words.length;i++){
            for(int j = 0;j<words[i].length()/2;j++){
                if(words[i].charAt(j) != words[i].charAt(words[i].length() - j - 1 )){
                    flag = false;
                    break;
                }
            }
            if(flag == true){
                System.out.printf(words[i]+ " ");
            }
            flag = true;
        }
    }
}